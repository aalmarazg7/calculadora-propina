export const calculoPropina = (subTotal: number, incluirPropina: boolean, porcentajePropina: number,
  dividirCuenta: boolean, dividirCuentaEntre:number )=> {
    //let resultado = 0;
    let importe = 0
    let propina = 0;
    let totalMasPropina = 0;
    let totalXPersona = 0;
    if(subTotal){
      importe=subTotal
    }
    if(incluirPropina){
      propina = subTotal * (porcentajePropina / 100);
   
    } else{
      importe = subTotal;
    }
    if (subTotal){
      totalMasPropina = subTotal + propina
    }

    if(dividirCuenta){
      totalXPersona =(importe + propina) / dividirCuentaEntre
    }else{
      totalXPersona = importe + propina
    }
    return {
      importe,
      propina,
      totalMasPropina,
      totalXPersona
    };

  };