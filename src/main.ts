import { calculoPropina } from "./bussines";
import { mostrarResultado } from "./presentation";

//definimos variables y su tipo

let subTotal = 0;
let incluirPropina = false;
let porcentajePropina = 10;
let dividirCuenta = false;
let dividirCuentaEntre = 2;


const renderizar = () => {
  const calculo = calculoPropina(subTotal, incluirPropina, porcentajePropina, dividirCuenta, dividirCuentaEntre);
  mostrarResultado(calculo, dividirCuenta)
}


document.getElementById('calculadora-form')?.addEventListener('submit', (e) => {
  e.preventDefault()
})
// listester para el subtotal
document.getElementById('sub-total')?.addEventListener('keyup', () => {
  const inputSubTotal = (document.querySelector('#sub-total') as HTMLInputElement).value;
  const textoError = document.querySelector('.error-input-subtotal') as HTMLElement;
  subTotal = parseFloat(inputSubTotal)
 
  if (isNaN(subTotal)) {
    textoError.classList.add('visible');
  } else {
    textoError.classList.remove('visible');
    
    renderizar();
  }

})
//lister para el otro porcentaje de propina 
document.getElementById('otra-opcion-propina')?.addEventListener('keyup', () => {
  const inputOtroPorcentaje = (document.querySelector('#otra-opcion-propina') as HTMLInputElement).value;
  porcentajePropina = parseFloat(inputOtroPorcentaje)
  console.log(inputOtroPorcentaje);
  if (isNaN(porcentajePropina)) {
    porcentajePropina = 0
  }
  renderizar();

})


//listener para la parte de el boton activo de incluir propina 

document.getElementById('incluir-propina')?.addEventListener('click', (e) => {
  const incluirPropinaBoton = e.target as HTMLButtonElement
  incluirPropina = !incluirPropina
  if (incluirPropina) {
    incluirPropinaBoton.classList.add('active');
    incluirPropinaBoton.innerText = 'Sí'
  } else {
    incluirPropinaBoton.classList.remove('active');
    incluirPropinaBoton.innerText = 'No'
  }
  renderizar();
})
//listener para la parte de los porcentajes de la propina
// los radio button necesitan name, todos iguial para que funcione como un grupo

const propinaPorcentajeRadios = document.querySelectorAll('[name="propina-porcentanje"]');
const otroPorcentajePropina = document.querySelector('.otra-opcion-propina') as HTMLElement;

for (let i = 0; i < propinaPorcentajeRadios.length; i++) {
  propinaPorcentajeRadios[i].addEventListener('change', (e) => {
    const currentRadio = e.target as HTMLInputElement
    switch (currentRadio.id) {
      case 'propina-10': {
        porcentajePropina = 10
        otroPorcentajePropina.classList.remove('visible');
        break
      }
      case 'propina-15': {
        porcentajePropina = 15
        otroPorcentajePropina.classList.remove('visible');
        break
      }
      case 'propina-18': {
        porcentajePropina = 18
        otroPorcentajePropina.classList.remove('visible');
        break
      }
      case 'propina-20': {
        porcentajePropina = 20
        otroPorcentajePropina.classList.remove('visible');
        break
      }
      case 'propina-otro': {
        porcentajePropina = 0;
        (document.querySelector('#otra-opcion-propina') as HTMLInputElement).value = '0'
        otroPorcentajePropina.classList.add('visible');
        break
      }
    }
    renderizar();
  })
}



//listener para la parte de el boton activo de dividir la cuenta

document.getElementById('dividir-cuenta')?.addEventListener('click', (e) => {
  const dividirCuentaBoton = e.target as HTMLButtonElement
  dividirCuenta = !dividirCuenta
  if (dividirCuenta) {
    dividirCuentaBoton.classList.add('active');
    dividirCuentaBoton.innerText = 'Sí'
  } else {
    dividirCuentaBoton.classList.remove('active');
    dividirCuentaBoton.innerText = 'No'
  }
  renderizar();
});

//lister para el otra opción de dividir propina
document.getElementById('otra-opcion-dividir-cuenta')?.addEventListener('keyup', () => {
  const inputOtraDivicionCuenta = (document.querySelector('#otra-opcion-dividir-cuenta') as HTMLInputElement).value;
  dividirCuentaEntre = parseInt(inputOtraDivicionCuenta)
  console.log(inputOtraDivicionCuenta);
  if (isNaN(dividirCuentaEntre)) {
    dividirCuentaEntre = 1
  }
  renderizar();

})

//listener para la parte de la división de la cuenta 

const dividirLaCuentaRadios = document.querySelectorAll('[name ="division"]');
const otraOpcionDividirCuentaEntre = document.querySelector('.otra-opcion-dividir') as HTMLElement;

for (let i = 0; i < dividirLaCuentaRadios.length; i++) {
  dividirLaCuentaRadios[i].addEventListener('change', (e) => {
    const dividirCuentaEntreRadios = e.target as HTMLInputElement
    switch (dividirCuentaEntreRadios.id) {
      case 'dividir-cuenta-2': {
        dividirCuentaEntre = 2
        otraOpcionDividirCuentaEntre.classList.remove('visible');
        break
      }
      case 'dividir-cuenta-3': {
        dividirCuentaEntre = 3
        otraOpcionDividirCuentaEntre.classList.remove('visible');
        break
      }
      case 'dividir-cuenta-4': {
        dividirCuentaEntre = 4
        otraOpcionDividirCuentaEntre.classList.remove('visible');
        break
      }
      case 'dividir-cuenta-5': {
        dividirCuentaEntre = 5
        otraOpcionDividirCuentaEntre.classList.remove('visible');
        break
      }
      case 'dividir-cuenta-otro': {
        dividirCuentaEntre = 1;
        (document.querySelector('#otra-opcion-dividir-cuenta') as HTMLInputElement).value = '1'
        otraOpcionDividirCuentaEntre.classList.add('visible');
        break
      }

    }

   renderizar();

  })
}



renderizar();