import { Totales } from './type';
const formatNumber = new Intl.NumberFormat('es-MX', { style: 'currency', currency: 'MXN' })
export const mostrarResultado = (resultados: Totales, dividirCuenta: boolean) => {
  let mensaje = `
  
  <span>Subtotal:</span>
  <span>${formatNumber.format(resultados.importe)}</span>
  <span>Propina: </span>
  <span>${formatNumber.format(resultados.propina)}</span>
  <span>Total por pagar: </span>
  <span>${formatNumber.format(resultados.totalMasPropina)}</span>`;
  if (dividirCuenta) {
    mensaje = `${mensaje}
    <span>Total por persona: </span>
    <span>${formatNumber.format(resultados.totalXPersona)}</span>`;
  }
  document.getElementById('total')!.innerHTML = mensaje

};

