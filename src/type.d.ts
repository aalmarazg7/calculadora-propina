export type Totales = {
    importe: number;
    propina: number;
    totalMasPropina : number;
    totalXPersona: number;
}

